﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    public GameObject player;

    public Vector3 offset;
    public Vector3 rotOffset;

    private Quaternion rotQuat;

    private Vector3 position;
    private Module currModule;

    private bool isFlying = false;
    private float xCam = 0;
    private float zCam = 0;

    private bool XDir = true;
    Vector3 last = new Vector3();

    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        




    }

    

    public void updateOffset()
    {
        //this.offset *= currModule.getEulerCurrNextDir();
    }

    public void restrictCameraX()
    {
        if (Mathf.Abs(player.transform.position.x - currModule.transform.position.x) > 20)
        {

            position = new Vector3(xCam, 0, player.transform.position.z);

        }
        else
        {
            xCam = player.transform.position.x;
            position = new Vector3(xCam, 0, player.transform.position.z);

        }
    }

    public void restrictCameraZ()
    {
        if (Mathf.Abs(player.transform.position.z - currModule.transform.position.z) > 20)
        {

            position = new Vector3(player.transform.position.x, 0, zCam);

        }
        else
        {
            xCam = player.transform.position.x;
            position = new Vector3(xCam, 0, player.transform.position.z);

        }
    }

    public void setOffset(Vector3 offset)
    {
        this.offset = offset;
    }

    public Vector3 getOffset()
    {
        return this.offset;
    }

    public void setModule(Module currModule)
    {
        if (this.currModule && currModule.getDir() != this.currModule.getDir())
        {
            XDir = !XDir;
        }
        //print("UPDATED");
        this.currModule = currModule;
    }
    
    public void setIsFlying(bool isFlying)
    {
        this.isFlying = isFlying;
    }

    public static Vector3 getInverse(Vector3 direction)
    {
        if (direction.x != 0)
        {
            return new Vector3(0, 0, 1);

        }
        else
        {
            return new Vector3(1, 0, 0);
        }

    }


}