﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



//Module starts as 10 length and x width
// so scale
public class Module : MonoBehaviour
{
    //length of the module
    public static int LENGTH = 500;
    
    //length of end block
    public static int BLOCK_LENGTH = 100;
    
    
    //width of the module. NOTE this shouldn't be changed as it is hard set in the prefab
    //static int RANDWIDTH = 20;
    public int numObstacles;
    //public HashSet<int> objects;
    public GameObject endBlock;
    public GameObject endBlock_l;
    public GameObject endBlock_r;

    public float powerupLiklihood;

    private GameObject allObstacles;
    private GameObject allItems;

    public List<GameObject> scenery;

    public List<GameObject> obstacles = new List<GameObject>();

    public List<GameObject> items = new List<GameObject>();

    private Vector3 dir;
    private Vector3 start;
    private Vector3 end;
    private Vector3 pos;
    private Vector3 nextDir;
    private Vector3 curDir;

    
    //NOTE : this is the module length, must be fixed to this size as the prefabs are this size

    //todo enum types and difficulty
    
    public void Initialize(Vector3 pos, Vector3 curDir, Vector3 nextDir, int numObstacles, float width)
    {

        this.allObstacles = GameObject.Find("AllObstacles");

        this.dir = curDir;
        this.nextDir = nextDir;
        this.curDir = curDir;

        this.pos = pos;
        GameObject ground = transform.GetChild(0).gameObject;
        ground.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(2, ModuleController.SCALE/2.2f);

        start = pos - (LENGTH * curDir) / 2;
        end = pos + (LENGTH * curDir)/2;


        //Create the endblock with the correct facing walls
        

        spawnEndblock();
        spawnItems();
        spawnObstacles();
        spawnScenery();

    }

    public void spawnEndblock()
    {
        if (nextDir != curDir)
        {
            var angle = Vector3.Angle(curDir, nextDir);
            if (Module.isLeftTurn(curDir, nextDir))
            {
                endBlock = Instantiate(endBlock_l, end + curDir * (ModuleController.BLOCK_LEN) / 2, Quaternion.LookRotation(curDir)) as GameObject;
            }
            else
            {
                endBlock = Instantiate(endBlock_r, end + curDir * (ModuleController.BLOCK_LEN) / 2,  Quaternion.LookRotation(curDir)) as GameObject;
            }
        }
        else
        {
            Quaternion dir = Quaternion.LookRotation(curDir);
            endBlock = Instantiate(endBlock, end + curDir * (ModuleController.BLOCK_LEN) / 2, dir) as GameObject;
           
        }
    }

    /// <summary>
    /// Spawns all obstacles
    /// </summary>
    public void spawnObstacles()
    {
        for (int i = 0; i < numObstacles; i++)
        {
            
            float percent = (i / (float)numObstacles) + 0.2f;
            Vector3 objPos = start + percent * dir * LENGTH;
            spawnObject(objPos, obstacles[Random.Range(0, obstacles.Count)]);
        }

    }
    
    /// <summary>
    /// Spawns an object at a percentage of the module.
    /// The percentages should be entererd as l.t 1 floats
    /// </summary>
    /// <param name="lenPercent"></param>
    /// <param name="obj"></param>
    public void spawnObject(Vector3 objPos, GameObject obj)
    {
        
        if (dir.x != 0)
        {
            objPos.z += Random.Range(-10, 10);
        }else if (dir.z != 0)
        {
            objPos.x += Random.Range(-10, 10);
        }

        //Depending on the type instatiate it differently
        GameObject ob;
        if (obj.name == "c")
        {
            ob = Instantiate(obj, objPos, Quaternion.LookRotation(this.dir)) as GameObject;
        }else if(obj.name == "d")
        {
            objPos += new Vector3(0, 2, 0);
            ob = Instantiate(obj, objPos, Quaternion.LookRotation(this.dir)) as GameObject;
        }
        else if(obj.name == "f")
        {
            objPos += new Vector3(0, 2, 0);
            ob = Instantiate(obj, objPos, Quaternion.LookRotation(this.dir)) as GameObject;
        }
        else
        {
            objPos += new Vector3(0, 10 + Random.Range(-5,3), 0);
            ob = Instantiate(obj, objPos, Quaternion.LookRotation(this.dir)) as GameObject;
        }
            

        ob.transform.parent = this.transform;
        ob.GetComponent<Obstacle>().Initialize();

    }


    public void destroy()
    {
        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
        GameObject.Destroy(this.endBlock);
        GameObject.Destroy(this.gameObject);
    }

    public void spawnScenery()
    {
        //TODO 
    }


    /// <summary>
    /// Spawns all items
    /// </summary>
    public void spawnItems()
    {
        //print("here");
        if(Random.Range(0,1f) > powerupLiklihood)
        {
            Vector3 position = this.getRandomLocation();
            //TODO redo
            Quaternion rotation = Quaternion.Euler(Vector3.Scale(CameraController.getInverse(getDir()), new Vector3(90,0,90)));
            GameObject item = GameObject.Instantiate(items[0], this.transform.position+new Vector3(0,4,0), rotation) as GameObject;
        }
    }


    /// <summary>
    /// Gets the percent complete of a world position in this current
    /// module.
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public float getPercentageComplete(Vector3 pos)
    {
        return Mathf.InverseLerp(
            Vector3.Dot(dir, start),
            Vector3.Dot(dir, end), 
            Vector3.Dot(pos, dir));
    }
    
    public Vector3 getDir()
    {
        return dir;
    }

    public Vector3 getPos()
    {
        return pos;
    }

    
    public Vector3 getNextDir()
    {
        return nextDir;
    }

    /// <summary>
    /// Gets the euler angles of the current and next direction
    /// </summary>
    /// <returns></returns>
    public Vector3 getEulerCurrNextDir()
    {
        return new Vector3(0, Vector3.Angle(this.getDir(), this.getNextDir()), 0);
    }


    


    public Vector3 getRandomLocation()
    {
        //TODO get a random location within the module
        return new Vector3(0, 0, 0);
    }


    public void killObjects()
    {
        foreach(GameObject e in obstacles)
        {
            //GameObject.Destroy(e, 5);
        }
    }

    public static bool isLeftTurn(Vector3 oldDir, Vector3 newDir)
    {
        return Vector3.Cross(oldDir, newDir).y < 0;
    }


}
