﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModuleController : MonoBehaviour {


    public Game game;
    public GameObject player;

    private Module currModule;
    private Module nextModule;
    private Module newModule;

    //public List<GameObject> modules;
    
    Vector3 currDirection;
    Vector3 nextDirection;
    Vector3 next2Direction;

    //Script used to control the camera
    public CameraController cameraController;

    public ShipController shipController;

    public GameObject modulePrefab;

    private int moduleLength;

    //Length of the end block. 
    public static int BLOCK_LEN = 100;

    //Size of the defined module prefab, it is then stretched.
    public static int MOD_SIZE = 10;

    public int numObstacles;


    //Used to change the length of the module
    public static int SCALE = 50;

    private bool turning;

    public PlayerController playerController;

    private int modulesComplete = 0;

    private Queue<Module> modules = new Queue<Module>();
    //private Queue<GameObject> endblocks = new Queue<GameObject>();

    private Vector3 pos;

    private int height;

    public ArrayList directions;

    void Start ()
    {
        currDirection = Vector3.forward;
        nextDirection = getNextDirection(currDirection);
        next2Direction = getNextDirection(nextDirection);
        
        //First module
        currModule = LoadModule(Vector3.zero, Vector3.forward, nextDirection);
        modules.Enqueue(currModule);
        //Second Module
        pos = getNextModulePos(currDirection, nextDirection, currModule.getPos());
        nextModule = LoadModule(pos, nextDirection, next2Direction);
        modules.Enqueue(nextModule);
        
        //Third module
        pos = getNextModulePos(nextDirection, next2Direction, nextModule.getPos());
        newModule = LoadModule(pos, next2Direction, getNextDirection(next2Direction));
        modules.Enqueue(newModule);

    }



    // Update is called once per frame

	/// <summary>
    /// 
    /// </summary>
    void Update () {

        //Camera turning, if our current module isn't the same as the next module and we've finished it
        
        generateNextModule();
        
	    
	    turning = false;
        game.turning = false;
	    if (modules.Count > 4)
	    {
	        modules.Dequeue().destroy();
	    }

    }

    private void generateNextModule()
    {
        /**
         * Code for generating each new module, we generate 2 ahead of current position
         * when 99% of the current module is completed by the player.
         */
        if (currModule.getPercentageComplete(playerController.transform.position) > 0.99)
        {
            modulesComplete++;
            

            if (!turning && currModule.getDir() != nextModule.getDir())
            {
                turning = true;
                game.turning = true;

                playerController.setNewDirection(currModule.getDir(), nextModule.getDir());
                playerController.addModuleCount();
                
            }
            
            
            
            currModule = nextModule;
            nextModule = newModule;
            
            cameraController.setModule(currModule);
            game.setCurrentModule(currModule);
            
            
            pos = getNextModulePos(nextModule.getDir(), nextModule.getNextDir(), nextModule.getPos());
            if (Random.Range(0, 100) > 50)
            {
                height += 10;
                //TODO add a setting for height management, this module will need to get the height ready for the next
                newModule = LoadModule(pos, nextModule.getNextDir(), getNextDirection(newModule.getNextDir()));

            }
            else
            {
                newModule = LoadModule(pos, nextModule.getNextDir(), getNextDirection(newModule.getNextDir()));
            }

            modules.Enqueue(newModule);
            
        }
    }

    Vector3 getNextDirection(Vector3 currDir)
    {
        
        float i = Random.Range(0, 100);
        if (currDir == Vector3.forward)
        {
            if (i < 33)
                return Vector3.left;
            else if (i < 66)
                return Vector3.right;
            return Vector3.forward;
        }
        if (currDir == Vector3.left)
        {
            if (i < 33)
                return Vector3.forward;
            else if (i < 66)
                return Vector3.left;
            return Vector3.back;
        }
        if (currDir == Vector3.right)
        {
            if (i < 33)
                return Vector3.forward;
            if (i < 66)
                return Vector3.right;
            return Vector3.back;
        }

        if (currDir == Vector3.back)
        {
            if (i < 33)
                return Vector3.left;
            else if (i < 66)
                return Vector3.right;
            return Vector3.back;
        }
        print("ERROR");
        return Vector3.forward;

    }

    Module LoadModule(Vector3 pos, Vector3 dir, Vector3 nextDir)
    {

        GameObject obj = Instantiate(modulePrefab, pos, Quaternion.LookRotation(dir)) as GameObject;
        Module mod = obj.GetComponent<Module>();
        mod.Initialize(pos, dir, nextDir, numObstacles, BLOCK_LEN);

        return mod;
    }


    private Vector3 getNextModulePos(Vector3 currDir, Vector3 nextDir, Vector3 modulePos)
    {
        

        Vector3 output;
        if (currDir == nextDir)
        {
            Vector3 newPos = modulePos + (currDir * Module.LENGTH) + (currDir * BLOCK_LEN);
            output = newPos;
        } else if(nextDir==Vector3.left || nextDir == Vector3.right) {
            Vector3 newPos = modulePos + (currDir * Module.LENGTH) / 2 + (currDir * BLOCK_LEN)/2 + (nextDir*Module.LENGTH)/2 + (nextDir*BLOCK_LEN)/2;
            output =  newPos;
        }
        else
        {
            Vector3 newPos = modulePos + (currDir * Module.LENGTH) / 2 + (currDir * BLOCK_LEN)/2 + (nextDir*Module.LENGTH)/2 + (nextDir*BLOCK_LEN)/2;
            output =  newPos;
        }

        return new Vector3(output.x, 0,output.z) + new Vector3(0,height,0);


    }

}


