﻿using UnityEngine;
using System.Collections;

public class ModuleLoader {
    public Coroutine coroutine { get; private set; }
    public object result;
    private IEnumerator target;
    public ModuleLoader(MonoBehaviour owner, IEnumerator target)
    {
        this.target = target;
        this.coroutine = owner.StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        while (target.MoveNext())
        {
            result = target.Current;
            yield return result;
        }
    }

}
