﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

    private Vector3 direction;
    private bool left;
    
    [Header("Side Move")]
    public bool sideMove;
    public int sideMax;
    int sideCount = 0;
    public float sideSpeed;

    [Space(10)]
    [Header("Spin")]
    public bool rotateMove;
    public bool spinX;
    public bool spinY;
    public bool spinZ;


    public void Initialize (bool left, int sideMax, int sideCount, float sideSpeed, Vector3 moduleDir) {
        direction = transform.position;
        this.left = left;
        this.sideMax = sideMax;
        this.sideCount = sideCount;
        this.sideSpeed = sideSpeed;
	}

    public void Initialize()
    {
        sideCount = sideMax / 2;
        if (Random.Range(0, 100)>50)
        {
            this.left = !left;
        }
    }
	
	
	void Update () {
        direction = new Vector3();

        if (sideMove)
        {
            moveLeft();
        }
        if (rotateMove)
        {
            spin();
        }
        transform.position = transform.position+ direction*Time.deltaTime*sideSpeed;

	}

    public void spin()
    {
        if (spinX)
        {
            transform.Rotate(new Vector3(1, 0, 0));
        }
        if (spinY)
        {
            transform.Rotate(new Vector3(0, 1, 0));
        }
        if (spinZ)
        {
            transform.Rotate(new Vector3(0, 0, 1));
        }
        
    }

    public void moveLeft()
    {
        if (left)
        {
            sideCount += 1;
            direction.x -= 1;
            if (sideCount > sideMax)
            {
                sideCount = 0;
                left = !left;
            }
        }if(!left)
        {
            sideCount += 1;
            direction.x += 1;
            if (sideCount > sideMax)
            {
                sideCount = 0;
                left = !left;
            }
        }

    }





}
