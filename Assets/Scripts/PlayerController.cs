﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float speed;
    Rigidbody rigid;
    public float jumpForce;
    public float sideSpeed;
    Vector3 currDirection;
    public GameObject bullet;
    public GameObject shooter;

    public float bulletSpeed;
    public float cameraRotateTime;

    private CharacterController controller;

    private CameraController cameraController;

    public UIController UIController;


    //Game class for handling highly coupled objects
    public Game game;


    public float gravity;
    public float vSpeed;

    public float constantSpeed;



    private int moduleCount;

	void Start () {
        cameraController = Camera.main.GetComponent<CameraController>();
        controller = GetComponent<CharacterController>();
        UIController.genText("hey man ;)", this.transform, TextFlyer.Type.Normal);
        
	}
	
	
	void Update () {
        
        playerControl();

    }

    void playerControl()
    {
       


        Vector3 direction = new Vector3();

        if (Input.GetKey(KeyCode.W))
        {
            direction += transform.forward;
            
        }
        if (Input.GetKey(KeyCode.A))
        {
            direction -= transform.right * sideSpeed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction -= transform.forward;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction += transform.right * sideSpeed;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            shoot();
        }
        if (controller.isGrounded)
        {
            vSpeed = -1;
            if (Input.GetButtonDown("Jump"))
            {
                vSpeed = jumpForce;
            }
        }
        direction += transform.forward * constantSpeed;

        vSpeed -= gravity * Time.deltaTime;
        direction.y = vSpeed;


        controller.Move(direction * speed * Time.deltaTime);
    }



    public void shoot()
    {

        GameObject projectile = Instantiate(bullet, shooter.transform.position, Quaternion.identity) as GameObject;
        projectile.GetComponent<Rigidbody>().AddForce(this.transform.forward * bulletSpeed);
        Destroy(projectile, 2.0f);
    }


    public void setNewDirection(Vector3 oldDir, Vector3 newDir)
    {
        
        
        float newAngle = Mathf.Atan2(newDir.x, newDir.z) * Mathf.Rad2Deg;
        int turnFrames = 20;


        //cameraController.setOffset(Quaternion.Euler(0, newAngle, 0) * cameraController.getOffset());

        Vector3 targetDir = new Vector3(this.transform.rotation.x * Mathf.Rad2Deg * 2,
            newAngle, 0);

        Vector3 diff;

        if (isLeftTurn(oldDir, newDir))
        {
            diff = (targetDir - this.transform.localEulerAngles) / turnFrames;
            StartCoroutine(rotateSlowly(20, targetDir, diff));
            print("left");
        }
        else
        {
            
            diff = (targetDir - this.transform.localEulerAngles + new Vector3(0, 360, 0)) / turnFrames;
            print("right");
            if (this.transform.localEulerAngles.y < targetDir.y)
            {
                diff = (targetDir - this.transform.localEulerAngles) / turnFrames;
            }
            else
            {
                diff = (targetDir - this.transform.localEulerAngles + new Vector3(0, 360, 0)) / turnFrames;
            }

            print(this.transform.localEulerAngles);
                print(targetDir);
                print((targetDir - this.transform.localEulerAngles + new Vector3(0, 360, 0)));
                StartCoroutine(rotateSlowly(20, targetDir, diff));

        }

    }


    IEnumerator rotateSlowly(int frames, Vector3 targetDir, Vector3 diff)
    {

        while (frames > 0)
        {

            this.transform.localEulerAngles = this.transform.localEulerAngles + new Vector3(0, diff.y, 0);

            frames -= 1;

            yield return new WaitForEndOfFrame();
        }
    }

    //checks if a turn is left using cross product
    private bool isLeftTurn(Vector3 oldDir, Vector3 newDir)
    {
        return Vector3.Cross(oldDir, newDir).y < 0;
    }




    public bool IsGrounded(){
        return Physics.Raycast(transform.position, -Vector3.up,  1f);
    }


    public void setDir(Vector3 dir)
    {
        this.currDirection = dir;
    }

    public void setSpeed(float moduleCount)
    {
        if (speed < 100)
        {
            speed = speed + (moduleCount / 80) * speed;
        }else if (speed < 120)
        {
            speed = speed + (moduleCount / 400) * speed;
        }
        
    }

    public void addModuleCount()
    {
        this.moduleCount += 1;
    }

    public void setModuleCount(int i)
    {
        this.moduleCount = i;
    }


    void OnTriggerEnter(Collider col)
    {
        
        if (col.gameObject.tag == "Pickup")
        {
            Destroy(col.gameObject);
            game.swapMode();
            print("PICKUP");
        }
        
        
       
        //Destroy(gameObject); // destroy the projectile anyway
    }



}
