﻿using UnityEngine;
using System.Collections;

public class ShipCameraController : MonoBehaviour {

    public GameObject ship;
    public Vector3 offset;
    public ModuleController moduleController;
    private bool turning = false;
    public Game game;

	void Start () {
        
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (this.turning)
        {

        }
            this.transform.rotation = Quaternion.Euler(0, this.transform.eulerAngles.y, 0);
        
        Vector3 position = new Vector3(ship.transform.position.x, 0, ship.transform.position.z);
        this.transform.position = Vector3.Scale(position, game.getCurrentModule().getDir()) + offset;
    }




    public static Vector3 getInverse(Vector3 direction)
    {
        if (direction.x != 0)
        {
            return new Vector3(0, 0, 1);

        }
        else
        {
            return new Vector3(1, 0, 0);
        }

    }

    public void setTurning(bool val)
    {
        this.turning = val;
    }
}
