﻿using UnityEngine;
using System.Collections;

public class TextFlyer : MonoBehaviour {

    Transform playerTransform;

    TextMesh textMesh;

    private Vector3 offset;
    public float speed;

    public enum Type
    {
        Spin,
        Normal,
        SlideInOff
    }
    

	public void Initialize(string text, Transform playerTransform, Type type)
    {

        this.textMesh = GetComponent<TextMesh>();
        textMesh.text = text;
        this.playerTransform = playerTransform;
        

    }
	
	// Update is called once per frame
	void Update () {

        offset.x -= 1 * Time.deltaTime * speed;
        offset.z += 1 * Time.deltaTime * speed;
        this.transform.position = playerTransform.forward*80 + Vector3.up*20 + offset;
        
        
        

	
	}
}
