﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

    public GameObject textPrefab;


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void genText(string text,Transform player, TextFlyer.Type type)
    {
        
        GameObject textObj = GameObject.Instantiate(textPrefab, transform.position, Quaternion.LookRotation(transform.forward)) as GameObject;
        textObj.GetComponent<TextFlyer>().Initialize(text, player, type);
    }
}
