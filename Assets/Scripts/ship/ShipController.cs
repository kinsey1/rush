﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {


    public GameObject player;
    private bool turning;
    public Game game;


	void Start () {
	
	}

    public float movementSpeed = 1.0f;
    public int invert = -1;
    

	
	// Update is called once per frame
	void Update () {
        if (this.isActiveAndEnabled)
        {
            player.transform.position = this.transform.position;
        }

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        print(horizontal);

        Vector3 direction = new Vector3(horizontal, invert * vertical, 1.0f);
        Vector3 finalDirection = new Vector3(horizontal, invert * vertical, 1.0f);

        transform.position += this.transform.forward * movementSpeed * Time.deltaTime;
        print(this.transform.forward);
        if (!game.turning)
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), 30f);
        
    }

    public void setNewDirection(Vector3 oldDir, Vector3 newDir)
    {
        print("ROTATING");

        float newAngle = Mathf.Atan2(newDir.x, newDir.z) * Mathf.Rad2Deg;
        int turnFrames = 20;

        print("HERE");
        print(newAngle);

        //cameraController.setOffset(Quaternion.Euler(0, newAngle, 0) * cameraController.getOffset());

        Vector3 targetDir = new Vector3(this.transform.rotation.x * Mathf.Rad2Deg * 2,
            newAngle, 0);

        Vector3 diff;

        if (isLeftTurn(oldDir, newDir))
        {
            diff = (targetDir - this.transform.localEulerAngles) / turnFrames;
            StartCoroutine(rotateSlowly(20, targetDir, diff));

        }
        else
        {
            diff = (targetDir - this.transform.localEulerAngles + new Vector3(0, 360, 0)) / turnFrames;
            StartCoroutine(rotateSlowly(20, targetDir, diff));
        }

    }


    IEnumerator rotateSlowly(int frames, Vector3 targetDir, Vector3 diff)
    {

        while (frames > 0)
        {

            this.transform.localEulerAngles = this.transform.localEulerAngles + new Vector3(0, diff.y, 0);

            frames -= 1;

            yield return new WaitForEndOfFrame();
        }
    }

    //checks if a turn is left using cross product
    private bool isLeftTurn(Vector3 oldDir, Vector3 newDir)
    {
        return Vector3.Cross(oldDir, newDir).y < 0;
    }
}
